import os,sys,argparse
from shutil import copy2

# Define stuff
errors = []



def performCopy(filename, path, dest):

    t = path.replace('\\', ';')
    target = t.replace(':', '')
    print('Target folder name: {}'.format(target))
    try:

        # If category folder doesn't exist, create it
        if not os.path.exists(dest):
            os.makedirs(dest)
            print('Creating category folder {}'.format(dest))

        targetFolder = dest + '\\' + target
        print('Target folder for {} is {} '.format(filename, targetFolder))

        if not os.path.exists(targetFolder):
            print('Creating target folder {} '.format(targetFolder))
            os.makedirs(targetFolder)

        targetFile = path + '\\' + filename
        print('Copying {} to {}'.format(targetFile, targetFolder))
        copy2(targetFile, targetFolder)

    except OSError as why:
        errors.append((filename, dest, str(why)))


def createIndex(indexFile, srcDir):

    # Create an index file
    with open(indexFile, 'w') as f:
        for root, dirs, files in os.walk(srcDir):
            for file in files:
                f.write("%s\n" % os.path.join(os.path.realpath(root), file))
    f.close()


def process(srcDir, dstDir):

    print('Process method got src and dst as {} and {}'.format(srcDir, dstDir))
    imageDest = dstDir + "\\IMAGES"
    movieDest = dstDir + "\\MOVIES"
    musicDest = dstDir + "\\MUSIC"
    textDest = dstDir + "\\TEXT"
    archiveDest = dstDir + "\\ARCHIVES"
    zeroDest = dstDir + "\\0"

    # Process files based on index
    indexFile = dstDir + '\\index.txt'
    print('Going to create the index file as {}'.format(indexFile))
    createIndex(indexFile, srcDir)

    with open(indexFile) as f:
        content = f.read().splitlines()

    for item in content:
        parts = os.path.split(item)
        path = parts[0]
        filename = parts[1]
        print('Processing {} in {}'.format(filename, path))

        if filename != 'index.txt':
            if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.gif', '.tiff')):
                print('\nFile: {} identified as IMAGE'.format(filename))
                performCopy(filename, path, imageDest)
            elif filename.lower().endswith(('.mp3', '.flac', '.ape', '.alac', '.m4a', '.wma')):
                print('\nFile: {} identified as MUSIC'.format(filename))
                performCopy(filename, path, musicDest)
            elif filename.lower().endswith(('.avi', '.mkv', '.mp4', '.mov', '.wmv')):
                print('\nFile: {} identified as MOVIE'.format(filename))
                performCopy(filename, path, movieDest)
            elif filename.lower().endswith(('.txt', '.rtf', '.doc', '.docx', '.pdf')):
                print('\nFile: {} identified as TEXT'.format(filename))
                performCopy(filename, path, textDest)
            elif filename.lower().endswith(('.zip', '.7zip', '.rar')):
                print('\nFile: {} identified as ARCHIVE'.format(filename))
                performCopy(filename, path, archiveDest)
            else:
                print('\nFile: {} matches no category'.format(filename))
                performCopy(filename, path, zeroDest)


def query_yes_no(question, default="no"):

    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def main(argv):

    parser = argparse.ArgumentParser()

    # add arguments to the parser
    parser.add_argument("srcDir")
    parser.add_argument("dstDir")

    # parse the arguments
    args = parser.parse_args()

    # get the arguments value
    if args.srcDir and args.dstDir:
        srcDir = args.srcDir
        dstDir = args.dstDir
        print('\nsrcDir: {}\ndstDir: {}\n'.format(srcDir, dstDir))
        if query_yes_no("Proceed?", None):
            print('\nYou said yes')
            process(srcDir, dstDir)
        else:
            print('\nCiao')


if __name__ == '__main__':
    main(sys.argv[1:])
